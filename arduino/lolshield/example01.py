"""
example01.py
Eric Pavey - www.akeric.com - 2010-08-03
Free to use, enjoy.

Simple example Pygame application using pylolgraph.py
Creates a small window (about the size of the lolshield on my laptop) that
draws a white 'donut' when the LMB is pressed.  These graphics are then displayed
on the lolshield\Arduino, presuming pylolgraph.pde has been installed on it.
"""

__ver__ = '1.0'
import sys
import pylolgraph

import pygame
from pygame.locals import *
pygame.init()

#--------------
# Constants

# Match that of lolshield:
WIDTH, HEIGHT = (112, 72)
FRAMERATE = 30
# Baud must match that of Arduino\lolshield sketch:
# 300, 1200, 2400, 4800, 9600, 14400, 19200 or 28800: Values greater than
# this seem to fail.
BAUD = 28800
# Port the Arduino\lolshield is on my winXP box:
#PORT = 'COM4'
# Port the Arduino is on, on my Raspberry Pi:
PORT = '/dev/ttyACM0'

#-------------
# screenSurf Setup
screenSurf = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("pylolgraph example 01")
clock = pygame.time.Clock()

lolshield = pylolgraph.LolShield(port=PORT, baudrate=BAUD)
lolshield.setSurface(screenSurf)

#------------
# Main Program
def main():
    print "Running Python version:", sys.version
    print "Running PyGame version:", pygame.ver
    print "Running pylolgraph version:", pylolgraph.__version__
    looping = True

    # Main loop
    while looping:
        print "loop!"
        # Maintain our framerate:
        clock.tick(FRAMERATE)

        screenSurf.fill(Color('black'))

        #-------------------------------
        # detect for events

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                looping = False

        mouseButtons = pygame.mouse.get_pressed()
        if mouseButtons[0]:
            # Make a nice donut on screen when the LMB is pressed:
            pygame.draw.circle(screenSurf, Color('white'), pygame.mouse.get_pos(), 128)
            pygame.draw.circle(screenSurf, Color('black'), pygame.mouse.get_pos(), 64)

        #-------------------------------
        # Update the pixels to be sent to the lolshield, and send them:
        lolshield.sendData()

        #-------------------------------
        # update our display:
        pygame.display.update()
        print "\tloop over"


#------------
# Execution from shell\icon:
if __name__ == "__main__":
    sys.exit(main())