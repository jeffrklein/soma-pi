# A port of the original Arduino code:
#  http://arduino.cc/en/Tutorial/Fade
"""
int led = 9;           // the pin that the LED is attached to
int brightness = 0;    // how bright the LED is
int fadeAmount = 5;    // how many points to fade the LED by

// the setup routine runs once when you press reset:
void setup()  { 
  // declare pin 9 to be an output:
  pinMode(led, OUTPUT);
} 

// the loop routine runs over and over again forever:
void loop()  { 
  // set the brightness of pin 9:
  analogWrite(led, brightness);    

  // change the brightness for next time through the loop:
  brightness = brightness + fadeAmount;

  // reverse the direction of the fading at the ends of the fade: 
  if (brightness == 0 || brightness == 255) {
    fadeAmount = -fadeAmount ; 
  }     
  // wait for 30 milliseconds to see the dimming effect    
  delay(30);                            
}
"""
from nanpy import Arduino as A

led = 9
brightness = 0 
fadeAmount = 5

A.pinMode(led, A.OUTPUT)

while True:
    # set the brightness of pin 9:
    A.analogWrite(led, brightness)
    
    # change the brightness for next time through the loop:
    brightness += fadeAmount
    
    # reverse the direction of the fading at the ends of the fade: 
    if brightness == 0 or brightness == 255:
        fadeAmount = -fadeAmount
        
    # wait for 30 milliseconds to see the dimming effect   
    A.delay(30)
    
