# A port of the original Arduino code:
# http://arduino.cc/en/Tutorial/Blink
"""  
int led = 13;

// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
  pinMode(led, OUTPUT);     
}

// the loop routine runs over and over again forever:
void loop() {
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);               // wait for a second
  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);               // wait for a second
}
"""
from nanpy import Arduino as A

led = 13

A.pinMode(led, A.OUTPUT)

while True:
    A.digitalWrite(led, A.HIGH);   # turn the LED on (HIGH is the voltage level)
    print "blink on"
    A.delay(1000);                 # wait for a second
    A.digitalWrite(led, A.LOW);    # turn the LED off by making the voltage LOW
    print "blink off"
    A.delay(1000);  