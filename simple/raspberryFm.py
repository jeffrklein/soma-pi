#!/user/bin/env python
"""
raspberryFm.py

This is the "old"\"first" RaspberryFM.  It's been replaced with raspberryFm01.py

online docs:  http://linux.die.net/man/1/mplayer
Get list of key constants : mplayer -input keylist
Get list of commands : mplayer -input cmdlist
Doc of command descriptions : http://www.mplayerhq.hu/DOCS/tech/slave.txt

mplayer commandline defaults:
< : previous in playlist
> : next in playlist
9 : decrease volume
0 : increase volume 
SPACE : pause

Requires mplayer & pyradio:
$ sudo apt-get install mplayer
Install PIP if you don't have it:
$ sudo apt-get install python-pip
$ sudo pip install pyradio
"""
import subprocess
import pygame
from pygame.locals import *
pygame.init()

GROOVESALAD = "http://ice.somafm.com/groovesalad"
#GROOVESALAD = "http://streamer-dtc-aa01.somafm.com:80/stream/1018"
LUSH = "http://ice.somafm.com/lush"
BEATBLENDER = "http://ice.somafm.com/beatblender"
DRONEZONE = "http://ice.somafm.com/dronezone"
pls = "/home/eric/mplayer/somafm.pls"

def main():
	"""
	Start the raspberryFm tuner....
	"""
	# If Pygame doesn't have a screen, it can't do keyboard detection...
	pygame.display.set_mode((32, 32))
	print "starting raspberryFm.py main()"
	process = subprocess.Popen("mplayer -slave -playlist %s"%pls, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
	while True:
		for event in pygame.event.get():
			if event.type == KEYDOWN:
				if event.key == K_UP:
					print "UP"
				elif event.key == K_DOWN:
					print "DOWN"
					process.communicate("quit\n")
					print "Radio Off!"
					return
				elif event.key == K_LEFT:
					print "LEFT"
					process.communicate("pt_step -1\n")
					#process.communicate("<")
				elif event.key == K_RIGHT:
					print "RIGHT"
					process.communicate("pt_step 1\n")
					#process.communicate(">")
				elif event.key == K_SPACE:
					print "SPACE"
	

if __name__ == "__main__":
	# try\except lets us cancle out while running in Raspberry Pi WebIDE:
	try:
		main()
	except KeyboardInterrupt:
		print "ctrl+c : Exit!"
