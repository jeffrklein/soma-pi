# Example #4 from http://picamera.readthedocs.org/en/release-1.0/quickstart.html

#  This example demonstrates starting a preview, setting some parameters and then 
# capturing an image while the preview is running:

import time
import picamera

with picamera.PiCamera() as camera:
    camera.resolution = (1280, 720)
    camera.start_preview()
    camera.exposure_compensation = 2
    camera.exposure_mode = 'spotlight'
    camera.meter_mode = 'matrix'
    camera.image_effect = 'gpen'
    # Give the camera some time to adjust to conditions
    time.sleep(2)
    camera.capture('foo.jpg')
    camera.stop_preview()
print "done"